package Pachet1;

import java.util.Scanner;

public class Ex1Si2Array {
        public static void main(String[] args) {

            //  Exercitiul1:
////  Sa se scrie un program ce verifica daca un array contine o anumita valoare.
////  Sa se creeze o metoda care primeste ca parametru un array de int si returneaza array-ul cu valorile citite de la tastatura.
////  Se citeste de la tastatura valoarea pe care dorim sa o cautam in array.
////  Se creeaza o metoda care returneaza true daca valoarea a fost gasita in array si false daca valoarea nu a fost gasita in array.
////  Se apeleaza metodele in metoda main pentru a se testa functionalitatea programului.

            int[] arrayEx1 = new int[3];
            Scanner scannEx1 = new Scanner(System.in);
            int[] arrayNou = arrayValoriCititeTastatura(arrayEx1, scannEx1);
            afisareArray(arrayNou);
            System.out.println();
            System.out.println("Ce valoare doriti sa stiti daca exista in array: ");
            int valoareCautata = scannEx1.nextInt();
            boolean rezultatValoareCautata;
            rezultatValoareCautata = valoareCautataTrueFalse(arrayNou, valoareCautata);
            if (rezultatValoareCautata) {
                System.out.println("Valoarea " + valoareCautata + " a fost gasita in array.");
            } else {
                System.out.println("Valoarea " + valoareCautata + " nu a fost gasita in array.");
            }

            //    Exercitiul2:
//    Sa se scrie un program care returneaza minimul si maximul valorilor din array.
//    Elementele array-ului sunt citite de la tastatura intr-o metoda ce returneaza un array cu valorile citite de la tastatura.
//    Sa se scrie o metoda care returneaza maximul array-ului si in metoda main se afiseaza acel maxim gasit.
//    Sa se scrie o metoda care returneaza minimul array-ului si in metoda main se afiseaza acel minim gasit.
//    Se va testa in metoda main functionalitatea programului.

            int[] arrayMinMax = new int[4];
            int[] arrayNou1 = arrayValoriCititeTastatura(arrayEx1, scannEx1);
            afisareArray(arrayNou1);
            System.out.println();
            int valMax = afisareArrayMax(arrayNou1);
            System.out.println("Valoarea maxima a array-ului este: " + valMax);
            int valMin = afisareArrayMin(arrayNou1);
            System.out.println("Valoarea minima a array-ului este: " + valMin);
        }

        public static int[] arrayValoriCititeTastatura(int[] array1, Scanner scannEx1) {
            int[] arrayNou = new int[array1.length];
            System.out.println("Introduceti de la tastatura elementele array-ului");
            for (int i = 0; i < arrayNou.length; i++) {
                System.out.print("Element " + (i + 1) + ": ");
                arrayNou[i] = scannEx1.nextInt();
            }
            return arrayNou;
        }

        public static void afisareArray(int[] array) {
            System.out.println("Elementele array-ului cu valorile citite de la tastatura sunt: ");
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
        }

        public static boolean valoareCautataTrueFalse(int[] array3, int valCautata) {
            for (int i = 0; i < array3.length; i++) {
                if (array3[i] == valCautata) {
                    return true;
                }
            }
            return false;
        }

        public static int afisareArrayMax(int[] array4) {
            int max = array4[0];
            for (int i = 0; i < array4.length; i++) {
                if(array4[i] > max) {
                    max = array4[i];
                }
            }
            return max;
        }

        public static int afisareArrayMin(int[] array5) {
            int min = array5[0];
            for (int i = 0; i < array5.length; i++) {
                if(array5[i] < min) {
                    min = array5[i];
                }
            }
            return min;
        }
    }

