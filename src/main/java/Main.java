import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


//    Exercitiul2:
//        Scrieti un program care citeste 2 numere de la tastatura.
//        Sa se faca produsul numerelor si sa se salveze intr-o variabila.
//        Daca produsul numerelor este par sa se afiseze toate numerele care sunt divizibile cu 2
//        si sunt mai mici ca acel produs.

        Scanner scrtu1 = new Scanner(System.in);
        System.out.print("Introduceti nr1 = ");
        int nr1 = scrtu1.nextInt();
        System.out.print("Introduceti nr2 = ");
        int nr2 = scrtu1.nextInt();
        int prod = nr1 * nr2;
        int matchNumbers = 0;
        if (prod % 2 == 0) {
            while (matchNumbers < prod) {
                if (matchNumbers % 2 == 0) {
                    System.out.println(matchNumbers);
                }
                matchNumbers++;
            }
        } else {
            System.out.println("Produsul numerelor " + prod + " este impar.");
        }
    }
}
